import 'dart:async' show StreamController;
import 'package:flutter/animation.dart';
import 'my_paginated_table_bloc_data.dart';

/// T: type of items \
/// U: type of item's id \
/// V: type of config \
class MyPaginatedTableBloc<T, U, V> {

  V? _params;
  U? _firstId;
  U? _lastId;
  int? _rowCount;
  int _currentPage = -1;

  /// Pour `retry`
  bool? _lastDirection;

  /// Pour `refreshCurrentPage`
  Future<List<T>> Function()? _lastFetchBuilder;

  final Future<int> Function(V? params) getCount;

  /// For first pagination `firstId` and `lastId` is `null` \
  /// rowPerPage: \
  /// params: \
  /// firstId: For `prev` pagination \
  /// lastId: For `next` pagination \
  final Future<List<T>> Function(int rowPerPage, V? params, bool isNext,  U? firstId, U? lastId) onFetch;
  final U Function(T) getItemId;
  final int rowPerPage;

  final VoidCallback? onEmpty;

  final StreamController<MyPaginatedTableData<T>> _controller = StreamController();

  MyPaginatedTableBloc({
    V? initialParams,
    required this.getCount,
    required this.onFetch,
    required this.getItemId,
    required this.rowPerPage,
    this.onEmpty
  }): _params = initialParams {
    fetch(true);
  }

  V? get params => _params;
  Stream<MyPaginatedTableData<T>> get listen => _controller.stream;

  void changeParams(V? value) {
    _params = value;
    _rowCount = null;
    _currentPage = -1;
    _firstId = null;
    _lastId = null;
    fetch(true);
  }

  // FIXME ignore older future
  void fetch(bool isNext) async {
    // exit if all ready loading
    try {
      // Loading status
      _controller.sink.add(const MyPaginatedTableData());

      _rowCount ??= await getCount(_params);

      if (_rowCount == 0)
        onEmpty?.call();

      _lastDirection = isNext;
      _lastFetchBuilder = () => onFetch(
        rowPerPage,
        _params,
        isNext,
        _firstId,
        _lastId
      );

      final result = await _lastFetchBuilder!();

      isNext ? ++_currentPage : --_currentPage;

      if (result.isNotEmpty) {
        _firstId = getItemId(result.first);
        _lastId = getItemId(result.last);
      }

      _controller.sink.add(MyPaginatedTableData(data: MyPaginatedTableSuccessData(
        contents: result,
        rowCount: _rowCount!,
        page: _currentPage,
      )));
    } catch (err) {
      _controller.sink.add(MyPaginatedTableData(error: err));
    }
  }

  // FIXME ignore older future
  void refreshCurrentPage() async {

    if (_currentPage == -1) {
      retry();
      return;
    }

    // exit if all ready loading
    try {
       // Loading status
      _controller.sink.add(const MyPaginatedTableData());

      _rowCount ??= await getCount(_params);

      if (_rowCount == 0)
        onEmpty?.call();

      final result = await _lastFetchBuilder!();

      _controller.sink.add(MyPaginatedTableData(data: MyPaginatedTableSuccessData(
        contents: result,
        rowCount: _rowCount!,
        page: _currentPage,
      )));
    } catch (err) {
      _controller.sink.add(MyPaginatedTableData(error: err));
    }
  }

  void retry() {
    fetch(_lastDirection ?? true);
  }

  void reset() {
    changeParams(_params);
  }

  void dispose() {
    _controller.close();
  }

}
