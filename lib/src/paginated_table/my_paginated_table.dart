import 'dart:io';
import 'dart:math' show min;

import 'package:flutter/material.dart';
import 'package:my_table/src/future_state.dart';
import 'package:my_table/src/my_table_base.dart';
import 'package:my_table/src/my_table_row.dart';
import 'package:my_table/src/paginated_table/my_paginated_table_bloc.dart';
import 'package:shimmer/shimmer.dart';

import 'my_paginated_table_bloc_data.dart';

class MyPaginatedTable<T, U, V>  extends StatelessWidget {

  static const TextStyle _defaultTextStyle = TextStyle(
    color: Colors.black87
  );

  static const TextStyle _defaultHeaderTextStyle = TextStyle(
    color: Colors.grey,
    fontWeight: FontWeight.w600
  );

  final MyPaginatedTableBloc<T, U, V>? provider;
  final MyTableRow Function(T) rowBuilder;
  // final ValueChanged<int>? onPageChanged; // TODO utile ???

  // For table
  final MyTableRow header;
  final TextStyle headerTextStyle;

  final double? rowHeight;
  final AlignmentGeometry alignment;
  final Map<int, TableColumnWidth>? columnWidths;
  final TableColumnWidth defaultColumnWidth;
  final TableBorder? border;
  final TextStyle textStyle;

  final double? paginatedHeight;
  final Decoration? paginatedDecoration;

  final Widget Function(V? params)? emptyWidgetBuilder;

  /// Lorsque flutter n'a pas encore termine de calculer le rowPerPage on utilise cette variable
  final int defaultLoadingRowCount;

  const MyPaginatedTable({
    super.key,
    required this.provider,
    required this.rowBuilder,
    required this.header,
    this.headerTextStyle = _defaultHeaderTextStyle,
    this.rowHeight = 46.0,
    this.alignment = Alignment.centerLeft,
    this.border = const TableBorder(horizontalInside: BorderSide(color: Colors.black12)),
    this.columnWidths,
    this.defaultColumnWidth = const FlexColumnWidth(),
    this.textStyle = _defaultTextStyle,
    this.paginatedHeight,
    this.paginatedDecoration,
    this.emptyWidgetBuilder,
    this.defaultLoadingRowCount = 1
  });

  void _onPageChanged(bool isNext) {
    provider!.fetch(isNext);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<MyPaginatedTableData<T>>(
      stream: provider?.listen,
      builder: (context, snapshot) {
        final data = snapshot.data;

        if (emptyWidgetBuilder != null && data?.state == FutureState.success && data!.data!.contents.isEmpty)
          return emptyWidgetBuilder!(provider!.params);

        var rowTable = MyTableBase(
          rowHeight: rowHeight,
          rows: _getRows(data),
          alignment: Alignment.centerLeft,
          border: border,
          columnWidths: columnWidths,
          defaultColumnWidth: defaultColumnWidth,
          textStyle: textStyle
        );

        return Column(
          children: [

            MyTableBase(
              rowHeight: rowHeight,
              rows: [header],
              alignment: Alignment.centerLeft,
              border: border,
              columnWidths: columnWidths,
              defaultColumnWidth: defaultColumnWidth,
              textStyle: headerTextStyle
            ),

            if (data?.state == null || data?.state == FutureState.waiting)
              Shimmer.fromColors(
                baseColor: Colors.grey.shade300,
                highlightColor: Colors.white,
                child: rowTable
              )
            else
              rowTable,

            Container(
              height: paginatedHeight ?? rowHeight,
              decoration: paginatedDecoration,
              child: data?.state == FutureState.success
                ? _getPaginatedComponent(data!.data!)
                : const SizedBox(width: double.infinity)
            )

          ],
        );
      }
    );
  }

  List<MyTableRow> _getRows(MyPaginatedTableData<T>? data) {
    final state = data?.state;
    if (state == FutureState.success) {
      final contents = data!.data!.contents;
      return List.generate(contents.length, (index) => rowBuilder(contents[index]), growable: false);
    }
    // isLaoding if state == FutureState.waiting || state == null
    return _fillRow(data?.error);
  }

  Widget _getPaginatedComponent(MyPaginatedTableSuccessData<T> data) {
    final int start = data.rowCount == 0 ? 0 : (data.page * provider!.rowPerPage) + 1;
    final int end = min(start + provider!.rowPerPage - 1, data.rowCount);

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [

        const SizedBox(width: 32.0),

        Text(
          '$start–$end sur ${data.rowCount}'
        ),

        const SizedBox(width: 32.0),

        IconButton(
          tooltip: 'Page précédente',
          onPressed: start < 2 ? null : () => _onPageChanged(false), // if start  == 0 || start == 1
          padding: EdgeInsets.zero,
          icon: const Icon(Icons.arrow_back_ios_new_rounded, size: 18.0)
        ),

        const SizedBox(width: 24.0),

        IconButton(
          tooltip: 'Page suivante',
          onPressed: end == data.rowCount ? null : () => _onPageChanged(true),
          padding: EdgeInsets.zero,
          icon: const Icon(Icons.arrow_forward_ios_rounded, size: 18.0)
        ),

        const SizedBox(width: 14.0),

      ],
    );
  }

  List<MyTableRow> _fillRow(Object? err) {

    if (err == null) { // Loading
      var child = Container(
        width: double.infinity,
        height: 16.0,
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        decoration: const BoxDecoration(
          color: Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(4.0))
        ),
      );

      return List<MyTableRow>.generate(
        provider?.rowPerPage ?? 1,
        (index) => MyTableRow(
          children: List<Widget>.generate(header.children.length, (_) => child, growable: false)
        ),
        growable: false
      );
    }

    Widget child;

    if (err is SocketException) { // Network error
      child = Center(
        child: TextButton.icon(
          onPressed: provider?.retry,
          icon: const Icon(Icons.refresh_rounded, color: Colors.red),
          label: const Text(
            'Veuillez vérifier votre connexion internet et réessayer',
            style: TextStyle(color: Colors.red),
          ),
        ),
      );
    } else { // Other error
      child = Center(
        child: TextButton.icon(
          onPressed: provider?.retry,
          icon: const Icon(Icons.error_outline, color: Colors.red),
          label: const Text(
            'Une erreur s\'est produite',
            style: TextStyle(color: Colors.red),
          ),
        ),
      );
    }

    return List<MyTableRow>.generate(
      1,
      (index) => MyTableRow(
        height: 128.0,
        children: List<Widget>.generate(1, (_) => child, growable: false)
      ),
      growable: false
    );
  }

}
