import 'package:my_table/src/future_state.dart';

class MyPaginatedTableSuccessData<T> {

  final List<T> contents;
  final int rowCount;
  final int page;

  MyPaginatedTableSuccessData({
    required this.contents,
    required this.rowCount,
    required this.page
  });

}

class MyPaginatedTableData<T> {

  final MyPaginatedTableSuccessData<T>? data;
  final Object? error;

  const MyPaginatedTableData({
    this.data,
    this.error
  });

  FutureState get state => data != null
    ? FutureState.success
    : error != null
      ? FutureState.error
      : FutureState.waiting;
}