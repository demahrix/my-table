import 'package:flutter/material.dart';
import 'my_table_base.dart';
import 'my_table_row.dart';

class MyScrollableTable extends MyTableBase {

  final MyTableRow header;
  final TextStyle headerTextStyle;

  static const TextStyle _defaultHeaderTextStyle = TextStyle(
    color: Colors.grey,
    fontWeight: FontWeight.w600
  );

  const MyScrollableTable({
    super.key,
    super.rowHeight,
    required super.rows,
    super.alignment,
    super.border,
    super.columnWidths,
    super.defaultColumnWidth,
    super.textStyle,
    required this.header,
    this.headerTextStyle = _defaultHeaderTextStyle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MyTableBase(
          rowHeight: super.rowHeight,
          rows: [header],
          alignment: Alignment.centerLeft,
          border: super.border,
          columnWidths: super.columnWidths,
          defaultColumnWidth: super.defaultColumnWidth,
          textStyle: _defaultHeaderTextStyle,
        ),
        Expanded(child: SingleChildScrollView(child: super.build(context)))
      ],
    );
  }
}
