import 'package:flutter/material.dart';
import 'my_table_base.dart';
import 'my_table_row.dart';

class MyTable extends MyTableBase {

  final MyTableRow? header;
  final TextStyle headerTextStyle;

  static const TextStyle _defaultHeaderTextStyle = TextStyle(
    color: Colors.grey,
    fontWeight: FontWeight.w600
  );

  const MyTable({
    super.key,
    super.rowHeight,
    required super.rows,
    super.alignment,
    super.border,
    super.columnWidths,
    super.defaultColumnWidth,
    super.textStyle,
    this.header,
    this.headerTextStyle = _defaultHeaderTextStyle,
  });

  @override
  Widget build(BuildContext context) {

    final extraRow = header != null ? 1 : 0;

    return Table(
      defaultColumnWidth: defaultColumnWidth,
      columnWidths: columnWidths,
      border: border,
      children: List.generate(rows.length + extraRow, (index) {

        final isHeader = index == 0 && extraRow == 1;

        final data = isHeader
          ? header!
          : rows[index - extraRow];

        return MyTableRowComponent(
          key: data.key,
          height: data.height ?? rowHeight,
          decoration: data.decoration,
          onTap: data.onTap,
          children: data.children,
          alignment: data.alignment ?? alignment,
          textStyle: isHeader ? headerTextStyle : textStyle
        );
      }, growable: false),
    );
  }
}
