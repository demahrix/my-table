import 'package:flutter/material.dart';
import 'package:my_table/my_table.dart';

class MyTableBase extends StatelessWidget {

  final double? rowHeight;
  final List<MyTableRow> rows;
  final AlignmentGeometry alignment;
  final Map<int, TableColumnWidth>? columnWidths;
  final TableColumnWidth defaultColumnWidth;
  final TableBorder? border;
  final TextStyle textStyle;

  static const TextStyle _defaultTextStyle = TextStyle(
    color: Colors.black87
  );

  const MyTableBase({
    super.key,
    this.rowHeight = 46.0,
    required this.rows,
    this.alignment = Alignment.centerLeft,
    this.border = const TableBorder(horizontalInside: BorderSide(color: Colors.black12)),
    this.columnWidths,
    this.defaultColumnWidth = const FlexColumnWidth(),
    this.textStyle = _defaultTextStyle
  });

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultColumnWidth: defaultColumnWidth,
      columnWidths: columnWidths,
      border: border,
      children: List.generate(rows.length, (index) {
        final data = rows[index];
        return MyTableRowComponent(
          key: data.key,
          height: data.height ?? rowHeight,
          decoration: data.decoration,
          onTap: data.onTap,
          children: data.children,
          alignment: data.alignment ?? alignment,
          textStyle: textStyle
        );
      }, growable: false),
    );
  }
}
