import 'package:flutter/material.dart';

class MyTableRow {

  final LocalKey? key;
  final double? height;
  final List<Widget> children;
  final VoidCallback? onTap;
  final Decoration? decoration;
  final AlignmentGeometry? alignment;
  // final TextStyle? textStyle;

  const MyTableRow({
    this.key,
    this.height,
    required this.children,
    this.onTap,
    this.decoration,
    this.alignment,
    // this.textStyle
  });

}

// private
class MyTableRowComponent extends TableRow {

  MyTableRowComponent({
    LocalKey? key,
    double? height,
    required List<Widget> children,
    VoidCallback? onTap,
    Decoration? decoration,
    required AlignmentGeometry alignment,
    required TextStyle textStyle
  }): super(
        key: key,
        decoration: decoration,
        children: List.generate(children.length, (index) => TableRowInkWell(
          onTap: onTap,
          child: SizedBox(
            height: height,
            child: Padding(
              padding: EdgeInsets.only(left: index == 0 ? 8.0 : 0.0, right: index == children.length - 1 ? 8.0 : 0.0),
              child: Align(
                alignment: alignment,
                child: DefaultTextStyle(
                  style: textStyle,
                  child: children[index]
                ),
              ),
            ),
          ),
        ))
  );

}
